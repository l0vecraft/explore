import 'package:explore/ui/widgets/carousel/DestinationCarousel.dart';
import 'package:flutter/material.dart';

class DestinationsComponent extends StatelessWidget {
  List<Map<String, String>> _destinations = [
    {
      'name': 'africa',
      'image':
          'https://www.bharad.es/wp-content/uploads/2019/03/Paisajes-de-Africa-770x516.jpg'
    },
    {
      'name': 'asia',
      'image':
          'https://www.telegraph.co.uk/content/dam/Travel/2020/March/japan.jpg'
    },
    {
      'name': 'europa',
      'image':
          'https://www.hola.com/imagenes/viajes/2015020476632/paisajes-invernales-europa/0-306-859/a_Alemania_Rothenburg-ob-der-a.jpg?filter=ds75'
    },
    {
      'name': 'south america',
      'image':
          'https://viajes.nationalgeographic.com.es/medio/2017/05/30/guatape_32886714.jpg'
    },
    {
      'name': 'north america',
      'image':
          'https://www.hola.com/imagenes/viajes/2014091973730/joyas-maravillosas-america-norte/0-287-833/a_san-francisc-a.jpg'
    },
    {
      'name': 'oceania',
      'image':
          'https://imagenesdepaisajes.net/wp-content/uploads/2016/05/25_aguas_cristalinas_donde_banarse_antes_de_morir_284326524_1200x800-816x544.jpg'
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          Text(
            'Destinations diversity',
            style: TextStyle(fontSize: 30),
          ),
          DestinationCarousel(
            data: _destinations,
          )
        ],
      ),
    );
  }
}
