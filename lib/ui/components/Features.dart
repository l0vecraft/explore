import 'package:explore/ui/widgets/cards/FeatureCard.dart';
import 'package:flutter/material.dart';

class FeaturesSection extends StatelessWidget {
  final Size size;

  const FeaturesSection({Key key, this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const double _space = 30;
    return Container(
      // padding: EdgeInsets.only(left: size.width / 50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FeatureCard(
            image:
                'https://www.senderismoeuropa.com/wp-content/uploads/2014/10/trekking-senderismo-hiking-excursionismo2.jpg',
            title: 'Trekking',
          ),
          SizedBox(width: _space),
          FeatureCard(
            image:
                'https://assets.afcdn.com/story/20161017/989289_w980h638c1cx511cy250.jpg',
            title: 'Animales',
          ),
          SizedBox(width: _space),
          FeatureCard(
            image:
                'https://hipertextual.com/files/2019/11/hipertextual-impresionantes-fotografias-galardonadas-concurso-sociedad-britanica-ecologia-2019227038.jpg',
            title: 'Photography',
          )
        ],
      ),
    );
  }
}
