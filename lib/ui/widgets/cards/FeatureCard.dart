import 'package:flutter/material.dart';

class FeatureCard extends StatelessWidget {
  final String image;
  final String title;

  const FeatureCard({Key key, this.image, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * .3,
      width: size.width / 5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            height: size.height * .2,
            width: size.width / 4,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: NetworkImage(image), fit: BoxFit.cover)),
          ),
          SizedBox(height: 11),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
              fontSize: 17,
            ),
          ),
        ],
      ),
    );
    // return Card(
    //     child: Container(
    //         height: size.height * .3,
    //         width: size.width / 5,
    //         decoration: BoxDecoration(
    //             image: DecorationImage(
    //                 image: NetworkImage(image), fit: BoxFit.cover)),
    //         child: Stack(
    //           alignment: Alignment.bottomCenter,
    //           children: [
    //             Positioned(
    //               bottom: 1,
    //               child: Center(
    //                 child: Text(
    //                   title,
    //                   style: TextStyle(
    //                     fontWeight: FontWeight.bold,
    //                     color: Colors.black,
    //                     fontSize: 17.8,
    //                   ),
    //                 ),
    //               ),
    //             ),
    //             Positioned(
    //               bottom: 1,
    //               child: Text(
    //                 title,
    //                 style: TextStyle(
    //                   fontWeight: FontWeight.bold,
    //                   color: Colors.white,
    //                   fontSize: 17,
    //                 ),
    //               ),
    //             )
    //           ],
    //         )));
  }
}
