import 'package:flutter/material.dart';

class MiCarousel extends StatefulWidget {
  @override
  _MiCarouselState createState() => _MiCarouselState();
}

class _MiCarouselState extends State<MiCarousel> {
  PageController _pageController = PageController();
  double _currentPage = 0.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      width: 80,
      child: PageView(
        controller: _pageController,
        pageSnapping: true,
        onPageChanged: (int value) {
          setState(() {
            _currentPage = value.toDouble();
          });
        },
        children: [
          Container(
            color: Colors.blue,
            child: Center(
              child: Text('Pagina 1'),
            ),
          ),
          Container(
            color: Colors.pink,
            child: Center(
              child: Text('Pagina 2'),
            ),
          ),
          Container(
            color: Colors.purple,
            child: Center(
              child: Text('Pagina 3'),
            ),
          ),
          Container(
            color: Colors.green,
            child: Center(
              child: Text('Pagina 4'),
            ),
          )
        ],
      ),
      // child: PageView.builder(
      //   itemCount: 10,
      //   controller: _pageController,
      //   onPageChanged: (int value) {
      //     setState(() {
      //       _currentPage = value.toDouble();
      //       print(_currentPage);
      //     });
      //   },
      //   itemBuilder: (context, position) {
      //     if (position == _currentPage.floor()) {
      //       return Container(
      //         width: 50,
      //         height: 20,
      //         color: position % 2 == 0 ? Colors.pink : Colors.blue,
      //         child: Center(
      //           child: Text('Page $position'),
      //         ),
      //       );
      //     } else if (position == _currentPage.floor() + 1) {
      //       return Container(
      //         width: 50,
      //         height: 20,
      //         color: position % 2 == 0 ? Colors.pink : Colors.blue,
      //         child: Center(
      //           child: Text('Page $position'),
      //         ),
      //       );
      //     } else {
      //       return Container(
      //         width: 50,
      //         height: 20,
      //         color: position % 2 == 0 ? Colors.pink : Colors.blue,
      //         child: Center(
      //           child: Text('Page $position'),
      //         ),
      //       );
      //     }
      //   },
      // ),
    );
  }
}
