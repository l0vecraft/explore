import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DestinationCarousel extends StatefulWidget {
  final List<Map<String, String>> data;

  const DestinationCarousel({Key key, this.data}) : super(key: key);
  @override
  _DestinationCarouselState createState() => _DestinationCarouselState();
}

class _DestinationCarouselState extends State<DestinationCarousel> {
  int _index = 0;
  final double _meanScreen = 479.0;
  PageController _controller = PageController();

  List<Widget> _buildOptions(Size size, bool isSmall) {
    List<Widget> _tmp = [];
    for (var i = 0; i < widget.data.length; i++) {
      _tmp.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: Text(
          widget.data[i]["name"].toUpperCase(),
          style: TextStyle(
              fontSize: isSmall ? 10 : 16,
              fontWeight: FontWeight.bold,
              color: Colors.grey[400]),
        ),
      ));
    }
    return _tmp;
  }

  List<Widget> _toWidget(Size size) {
    return widget.data
        .map(
          (e) => Container(
              width: size.width / 1.1,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(e['image'].toString()),
                      fit: BoxFit.fill),
                  borderRadius: BorderRadius.circular(20))),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final bool smallD = size.width < _meanScreen;
    return Container(
        padding: EdgeInsets.symmetric(vertical: 30),
        child: Stack(
          children: [
            CarouselSlider(
                options: CarouselOptions(
                    carouselController: _controller,
                    onPageChanged: (int value, _) {
                      setState(() {
                        _index = value;
                      });
                    },
                    enlargeCenterPage: true,
                    height: 400,
                    aspectRatio: 18 / 8,
                    autoPlay: true,
                    enableInfiniteScroll: true,
                    viewportFraction: 0.8),
                items: _toWidget(size)),
            Center(
              heightFactor: 5.5,
              child: Text(
                widget.data[_index]['name'],
                style: GoogleFonts.electrolize(
                    letterSpacing: 8,
                    fontSize: size.width / 25,
                    color: Colors.white,
                    shadows: [
                      BoxShadow(
                          blurRadius: 2.6,
                          color: Colors.black,
                          offset: Offset(2, 3))
                    ]),
              ),
            ),
            Positioned(
                bottom: -1,
                left: smallD ? size.width / 11.2 : size.width / 4.5,
                child: Card(
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      height: size.height * .06,
                      width: smallD ? size.width * .8 : size.width / 1.8,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: _buildOptions(size, smallD),
                      )),
                ))
          ],
        ));
  }
}
