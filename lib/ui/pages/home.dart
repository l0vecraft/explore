import 'package:explore/ui/components/Features.dart';
import 'package:explore/ui/widgets/CustomOptions.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<bool> _onHoverList = [false, false, false];
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size(size.width, 1000),
        child: Container(
            child: Padding(
          padding: EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Explore'.toUpperCase()),
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                      onTap: () {},
                      onHover: (bool value) {
                        setState(() {
                          _onHoverList[0] = value;
                        });
                      },
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text('Discover',
                              style: TextStyle(
                                  color: _onHoverList[0]
                                      ? Colors.blue[100]
                                      : Colors.white,
                                  fontWeight: FontWeight.bold)),
                          SizedBox(height: 5),
                          Visibility(
                              maintainAnimation: true,
                              maintainState: true,
                              maintainSize: true,
                              visible: _onHoverList[0],
                              child: Container(
                                height: 2,
                                width: 30,
                                color: Colors.white,
                              ))
                        ],
                      )),
                  SizedBox(width: size.width / 20),
                  InkWell(
                    onTap: () {},
                    onHover: (bool value) {
                      setState(() {
                        _onHoverList[1] = value;
                      });
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('Contact Us',
                            style: TextStyle(
                                color: _onHoverList[1]
                                    ? Colors.blue[100]
                                    : Colors.white,
                                fontWeight: FontWeight.bold)),
                        SizedBox(height: 5),
                        Visibility(
                            maintainAnimation: true,
                            maintainState: true,
                            maintainSize: true,
                            visible: _onHoverList[1],
                            child: Container(
                              height: 2,
                              width: 30,
                              color: Colors.white,
                            ))
                      ],
                    ),
                  ),
                ],
              )),
              // SizedBox(width: size.width / 50),
              InkWell(
                onTap: () {},
                onHover: (bool value) {
                  setState(() {
                    _onHoverList[2] = value;
                  });
                },
                child: Text('Login',
                    style: TextStyle(
                        color: _onHoverList[2] ? Colors.white : Colors.black,
                        fontWeight: FontWeight.bold)),
              )
            ],
          ),
        )),
      ),
      body: Stack(
        children: [
          Container(
              child: SizedBox(
            height: size.height * .45,
            width: size.width,
            child: Image.asset(
              'assets/images/mountain.jpg',
              fit: BoxFit.cover,
            ),
          )),
          Center(
              heightFactor: 1,
              child: Padding(
                padding: EdgeInsets.only(
                    top: size.height * .4,
                    left: size.width / 5,
                    right: size.width / 5),
                child: Card(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    height: size.height * .11,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomOptions(
                          title: 'Destination',
                        ),
                        Container(
                            height: size.height * .08,
                            color: Colors.grey.withOpacity(.6),
                            width: 1.1),
                        CustomOptions(
                          title: 'Dates',
                        ),
                        Container(
                            height: size.height * .08,
                            color: Colors.grey.withOpacity(.6),
                            width: 1.1),
                        CustomOptions(
                          title: 'People',
                        ),
                        Container(
                            height: size.height * .08,
                            color: Colors.grey.withOpacity(.6),
                            width: 1.1),
                        CustomOptions(
                          title: 'Experience',
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          Center(
            child: Padding(
              padding: EdgeInsets.only(
                  top: size.height * .2,
                  left: size.width / 13,
                  right: size.width / 15),
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      'Featured',
                      style:
                          TextStyle(fontSize: 40, fontWeight: FontWeight.w500),
                    ),
                    Expanded(
                      child: Text(
                        'Unique wildlife tours & destinations',
                        textAlign: TextAlign.end,
                      ),
                    )
                  ]),
            ),
          ),
          Positioned(
              top: size.height * .65,
              child: Center(child: FeaturesSection(size: size)))
        ],
      ),
    );
  }
}
