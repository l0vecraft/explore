import 'package:explore/ui/components/Destinations.dart';
import 'package:explore/ui/components/Features.dart';
import 'package:explore/ui/widgets/CustomOptions.dart';
import 'package:explore/ui/widgets/carousel/MiCarousel.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: Colors.blue,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.asset(
                'assets/images/mountain.jpg',
                fit: BoxFit.cover,
              ),
            ),
            leadingWidth: 150,
            leading: Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 8),
              child: Text(
                'Explore'.toUpperCase(),
                style: TextStyle(color: Colors.black),
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: InkWell(
                  onTap: () {},
                  child: Text('Login',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                ),
              ),
              SizedBox(
                width: size.width / 45,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, right: 4),
                child: InkWell(
                  onTap: () {},
                  child: Text('Sign In',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                ),
              )
            ],
            title: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                        onTap: () {},
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Discover',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold)),
                          ],
                        )),
                    InkWell(
                        onTap: () {},
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Contact Us',
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                          ],
                        )),
                  ],
                ),
                SizedBox(height: size.height * .14),
                Card(
                  child: Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    height: 40,
                    width: size.width / 1.5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomOptions(
                          title: 'Destination',
                        ),
                        Container(
                            height: size.height * .08,
                            color: Colors.grey.withOpacity(.6),
                            width: 1.1),
                        CustomOptions(
                          title: 'Dates',
                        ),
                        Container(
                            height: size.height * .08,
                            color: Colors.grey.withOpacity(.6),
                            width: 1.1),
                        CustomOptions(
                          title: 'People',
                        ),
                        Container(
                            height: size.height * .08,
                            color: Colors.grey.withOpacity(.6),
                            width: 1.1),
                        CustomOptions(
                          title: 'Experience',
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            expandedHeight: size.height * .4,
            toolbarHeight: 200,
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  padding: EdgeInsets.only(
                      top: size.height * .2,
                      left: size.width / 13,
                      right: size.width / 15),
                  child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Featured',
                          style: TextStyle(
                              fontSize: 40, fontWeight: FontWeight.w500),
                        ),
                        Expanded(
                          child: Text(
                            'Unique wildlife tours & destinations',
                            textAlign: TextAlign.end,
                          ),
                        )
                      ]),
                ),
                FeaturesSection(size: size),
                DestinationsComponent(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
