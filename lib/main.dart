import 'package:explore/ui/pages/home.dart';
import 'package:explore/ui/pages/home2.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Explore',
      home: HomePage(),
    );
  }
}
